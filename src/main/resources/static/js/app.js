'use strict';

var training = angular.module('training', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/all', {
                controller: 'TableCtrl',
                templateUrl: 'js/partials/table.html'
            })

            .otherwise({redirectTo: '/all'});
    });
